#!/bin/bash

pip3 install requests
pip3 install pymongo

docker pull mongo:4.2.0
docker run -d -p 27017:27017 --name mongodb mongo:4.2.0

