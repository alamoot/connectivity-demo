
# SnapTravel Backend Challenge

Amir Fata's implementation of the SnapTravel backend code challenge.

## Overview

The solution is written in `python` (version `3`) and uses the `requests` package to make calls to the SnapTravel APIs.
The DB is `Mongo` and so the `pymongo` package is used to interact with it.
Mongo is set up, by pulling a docker image.

For info on the requirements of the solution, see the `snaptravel_instructions.md` file.

`service.py` includes all the logic for the solution, saves desired hotel data into a Mongo DB called `testDB`, under `hotels` collection.

`setup.sh` sets the solution up for usage.

## How to use:

To setup your machine, run `setup.sh` to setup your machine:
```Bash
bash setup.sh
```

**Note** : It's assumed that you have `python3`, `pip3` and `docker` setup.

The script does the following:

 1. Install `python3` packages `requests` and `pymongo` if don't already exist:

```Bash
pip3 install requests
pip3 install pymongo
```

 2. Set up the `mongo` DB:
 ```Bash
docker pull mongo:4.2.0
docker run -d -p 27017:27017 --name mongodb mongo:4.2.0
 ```
Make sure the port `27017` isn't already in use before running the shell script.

The solution should run on `python` versions `2` and `3`, but to set up versoin `2`, change `pip3`, to `pip` in `setup.sh`.

### Running:

Finally, run `service.py`. Pass the `city`, `checkin`, and `checkout` parameters in that order to `service.py`. Here is an example:
```Bash
python3 service.py Toronto 2019-11-11 2019-12-12
```
In addition to putting in date into the DB, the service will also print the number of entries added to DB, and their definitions.

## Assumptions:

 - Neither of the API responses include non unique hotels.
 - Having repeated hotels in the DB is fine as the instructions didn't specify otherwise. A hotel is considered to be repeated in DB if multiple hotels have the same `id`.
 - No need to sanity check the input, as specified in the instructions.
 - The runner of the solution has `python3`, `pip3`, and `docker` set up.
