import sys
import requests
import json
from xml.etree import ElementTree
from pymongo import MongoClient

# Set up MongoDB Client
mongo = MongoClient(port = 27017)

class snaptravelClient:
    NEW_URL = "https://experimentation.getsnaptravel.com/interview/hotels"
    OLD_URL = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"

    # Make JSON payload post call given the city, checkin, and checkout. Return the reponse
    def makeJsonCall(self, city, checkin, checkout):
        payload = {
            "city" : city,
            "checkin" : checkin,
            "checkout" : checkout,
            "provider" : "snaptravel",
        }
        response = requests.post(self.NEW_URL, json = payload)
        return response

    # Make XML payload post call given the city, checkin, and checkout. Return the reponse
    def makeXmlCall(self, city, checkin, checkout):
        xmlTemplate="""
        <?xml version="1.0" encoding="UTF-8"?>
        <root>
            <city>%(city)s</city>
            <checkin>%(checkin)s</checkin>
            <checkout>%(checkout)s</checkout>
            <provider>snaptravel</provider>
        </root>
        """
        
        xmlData = {
            'city' : city,
            'checkin' : checkin,
            'checkout' : checkout,
        }
        xmlPayload = xmlTemplate % xmlData

        response = requests.post(self.OLD_URL, data = xmlPayload, headers = {'Content-Type' : 'application/xml'})
        return response

# Given the JSON and XML responses from the SnapTravel APIs, returns the hotels common in responses
# The return value is a JSON array
def getCommonHotels(jsonResponse, xmlResponse):
    hotelPrices = dict() # Mapping: hotel id -> [snaptravel price, retail price]

    # Add all hotel id -> price mapping for the JSON response
    for hotel in jsonResponse.json()['hotels']:
        id = hotel["id"]
        if id not in hotelPrices:
            hotelPrices[id] = dict({"snaptravel" : hotel["price"]})
        # Not expecting repeated hotels
    
    # Add the XML hotel's price for hotels alreay in hotelPrices (ie. hotel was present in the JSON response)
    tree = ElementTree.fromstring(xmlResponse.content)
    for child in tree.findall('element'):
        id = int(child.find('id').text)
        price = float(child.find('price').text)
        if id in hotelPrices:
            hotelPrices[id]["retail"] = price
    
    # Remove entries from hotelPrices that are only in the JSON response (ie. hotels that were no present in both responses)
    hotelPrices = dict((k, v) for (k, v) in hotelPrices.items() if len(v) > 1)
    # print(hotelPrices)

    # Get all common hotels
    commonHotels = []
    for hotel in jsonResponse.json()['hotels']:
        id = hotel["id"]
        if id in hotelPrices:
            hotel["price"] = hotelPrices[id]
            commonHotels.append(hotel)
    
    return commonHotels

# Store results in the DB
def storeHotelsInDB(hotels):
    if hotels:
        oldCount = mongo.testDB.hotels.count_documents({})
        mongo.testDB.hotels.insert_many(hotels)
        newCount = mongo.testDB.hotels.count_documents({})

        print(newCount - oldCount, "new hotel(s) added to DB")
        for hotel in hotels:
            print(hotel)

# Run the solution
def execute():
    s = snaptravelClient()
    city = sys.argv[1]
    checkin = sys.argv[2]
    checkout = sys.argv[3]

    jsonResponse = s.makeJsonCall(city, checkin, checkout)
    xmlResponse = s.makeXmlCall(city, checkin, checkout)
    # print(jsonResponse, xmlResponse)

    commonHotels = getCommonHotels(jsonResponse, xmlResponse)
    storeHotelsInDB(commonHotels)

if __name__ == "__main__":
    execute()
